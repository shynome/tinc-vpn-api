const fse = require('fs-extra')
const path = require('path')
const uuidv4 = require('uuid/v4')

exports.TINC_DIR = process.env.TINC_DIR || process.env.tinc_dir || 'data'
exports.TINC_HOSTS_DIR = path.join(exports.TINC_DIR,'hosts')
exports.TOKEN_FILE = path.join(exports.TINC_DIR,'token.tinc-vpn-api')

exports.TOKEN = function getToken(){
  if(fse.pathExistsSync(exports.TOKEN_FILE)){
    return fse.readFileSync(exports.TOKEN_FILE,'utf8')
  }
  let token = process.env.TOKEN || process.env.token || uuidv4()
  fse.mkdirpSync(exports.TINC_HOSTS_DIR)
  fse.writeFileSync(exports.TOKEN_FILE, token)
  return token
}()

let PORT = Number(process.env.PORT || process.env.port)
exports.PORT = isNaN(PORT) ? 8080 : PORT
