const fse = require('fs-extra')
const path = require('path')
const { config: { TINC_HOSTS_DIR } } = require('@/index')

exports.Tinc = class Tinc {
  /**
   * @param {{hostname:string}} args 
   */
  static async getHost(args){
    let hostFilepath = path.join(TINC_HOSTS_DIR, args.hostname)
    let host = await fse.readFile(hostFilepath, 'utf8')
    return host
  }
  /**
   * @param {{hostname:string}} args 
   */
  static async delhost(args){
    if(args.hostname === 'server'){
      throw new Error('不能删除服务器 host 文件')
    }
    let hostFilepath = path.join(TINC_HOSTS_DIR, args.hostname)
    await fse.remove(hostFilepath)
    return args.hostname
  }
  /**
   * @param {{hostname:string, content:string, force:boolean}} args
   */
  static async addHost(args){
    let hostFilepath = path.join(TINC_HOSTS_DIR, args.hostname)
    if(args.content.length===0){
      throw new Error("content 内容不正确")
    }
    if(
      await fse.pathExists(hostFilepath)
      && !args.force
    ){
      throw new Error(`host: ${args.hostname} 已存在, 添加 force 参数强制覆盖`)
    }
    await fse.writeFile(hostFilepath,args.content)
    return await this.getHost({ hostname: args.hostname })
  }
  static async getHosts(){
    return await fse.readdir(TINC_HOSTS_DIR)
  } 
}