const { ApolloServer } = require('apollo-server')
const { schema } = require('@/schema')
const { config } = require('@/index')

const server = exports.server = new ApolloServer({
  schema: schema,
  context: ({req})=>{
    let token = req.headers.token || req.query.token
    if(token !== config.TOKEN){
      throw new Error('授权码不对')
    }
    return { token }
  }
})

void async function main(){
  if(!config.TOKEN || !config.TINC_DIR){
    throw new Error('请先设置环境变量 TOKEN 和 HOSTS_DIR')
  }
  const serverinfo = await server.listen(config.PORT,'0.0.0.0')
  console.log('server url:'+serverinfo.url)
}()
