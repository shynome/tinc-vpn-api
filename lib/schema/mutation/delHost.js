const graphql = require('graphql')
const { GraphQLString, GraphQLBoolean, GraphQLNonNull, } = graphql
const { Tinc: Host } = require('Common/Tinc')

module.exports = /**@type {graphql.GraphQLFieldConfig<any,any,any>} */({
  type: GraphQLNonNull(GraphQLString),
  args: {
    'hostname': {
      type: GraphQLNonNull(GraphQLString),
      description: "主机名"
    }
  },
  resolve: async (src,args)=>{
    return Host.delhost(args)
  }
})
