const { GraphQLObjectType } = require('graphql')

module.exports = new GraphQLObjectType({
  name: 'mutation',
  fields:{
    'addHost': require('./addHost'),
    'delHost': require('./delHost'),
  }
})
