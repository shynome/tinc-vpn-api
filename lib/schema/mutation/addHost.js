const graphql = require('graphql')
const { GraphQLString, GraphQLBoolean, GraphQLNonNull, } = graphql
const { Tinc: Host } = require('Common/Tinc')

module.exports = /**@type {graphql.GraphQLFieldConfig<any,any,any>} */({
  type: GraphQLNonNull(GraphQLString),
  args: {
    'hostname': {
      type: GraphQLNonNull(GraphQLString),
      description: "主机名"
    },
    "content": {
      type: GraphQLNonNull(GraphQLString),
      description: "host 文件内容",
    },
    'force':{
      type: GraphQLBoolean,
      description: "是否覆盖同名文件"
    },
  },
  resolve: async (src,args)=>{
    return Host.addHost(args)
  }
})
