const graphql = require('graphql')
const {
  GraphQLString, 
  GraphQLList,
  GraphQLNonNull,
} = graphql
const { Tinc: Host } = require('Common/Tinc')

module.exports = /**@type {graphql.GraphQLFieldConfig<any,any,any>} */({
  type: GraphQLNonNull(GraphQLList(GraphQLNonNull(GraphQLString))),
  description: "主机列表",
  resolve: async ()=>{
    return await Host.getHosts()
  }
})
