const { GraphQLObjectType } = require('graphql')

module.exports = new GraphQLObjectType({
  name: 'query',
  fields:{
    'hosts': require('./hosts'),
    'host': require('./host'),
  }
})
