const graphql = require('graphql')
const { GraphQLString, GraphQLNonNull } = graphql
const { Tinc: Host } = require('Common/Tinc')

module.exports = /**@type {graphql.GraphQLFieldConfig<any,any,any>} */({
  type: GraphQLNonNull(GraphQLString),
  description: "服务器主机的host",
  args:{
    'hostname': {
      type: GraphQLNonNull(GraphQLString),
      description: '要输出的 host 名字'
    }
  },
  /**
   * @param {{hostname:string}} args
   */
  resolve: async (src, args)=>{
    return Host.getHost(args)
  }
})
